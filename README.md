# EventScheduler #
The EventScheduler class implements a simple timer-based event scheduler.

Events are contained in an `EventContext` object and consist of an event handler callback function and a start time.  Events are scheduled by adding them to 
the event handler.  When the event start time matches the elapsed system time, the event handler function is called.

The scheduler can run in either polled or interrupt mode. In polled mode, event time is checked in the `EventScheduler::service()` method and all event handler functions are called from the this method. In interrupt mode, a timer is set to expire at the event's start time and calls an interrupt handler. If the `isr_run` flag is set in the event context, then the event handler function is called from the interrupt handler.  Running an event handler from an interrupt can provide precise timing actions, but must be used with caution. 

The `EventScheduler` base class provides a polling mechanism using the Arduino `millis()` function.  Interrupt modes must be implemented by creating a subclass and overriding the virtual methods `initTimer()`, `setTimer()` and `clearTimer()`.  The `getTime()` method can also be overriden in order to provide timing units consistent with the interrupt timer (microseconds, system ticks, etc.).


### EventScheduler64 ###
The `EventScheduler64` class provides an interrupt-driven event scheduler based on the `EventScheduler` base class, but uses `IntervalTimerEx` for timer interrupts and `micros64` for system time.  Events can be scheduled with microsecond precision without fear of system time rollover.  

The IntervalTimerEx library is currently only supported on Teensy platforms from PJRC.  
See https://www.pjrc.com/teensy/.  

The IntervalTimerEx library can be found here:  https://gitlab.com/tkerr/IntervalTimerEx  

The micros64 library can be found here:  https://gitlab.com/tkerr/micros64


### Dependencies ###
Uses the `PriorityQueue.h` template in the Queue library found here: https://gitlab.com/tkerr/Queue


### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License  
https://opensource.org/licenses/MIT
