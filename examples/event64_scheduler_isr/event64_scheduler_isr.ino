/******************************************************************************
 * event64_scheduler_isr.ino
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Example of using the EventScheduler64 class using timer interrupts.
 *
 * The EventScheduler64 class is uses the IntervalTimerEx and micros64 libraries
 * which are currently only supported on Teensy platforms from PJRC.  
 * See https://www.pjrc.com/teensy/.
 *
 * The IntervalTimerEx library can be found here:
 * https://gitlab.com/tkerr/IntervalTimerEx
 *
 * The micros64 library can be found here:
 * https://gitlab.com/tkerr/micros64
 */

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "arduino_arch.h"
#ifndef ARDUINO_ARCH_TEENSY
    #error "This sketch is only supported on Teensy platforms."
#endif

#include "EventScheduler64.h"
#include "micros64.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
void ledHandler();            //!< Event handler to turn LED on and off
void printOnceHandler();      //!< Event handler to print timestamps once
void printPeriodicHandler();  //!< Event handler to print recurring timestamps
void pastHandler();           //!< Event handler to demonstrate events in the past


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD         (115200)
#define SCHEDULE_SIZE           (16)
#define LED_PERIOD_US     (500000UL)
#define PRINT_PERIOD_US  (2359000UL)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

EventContext event_buf[SCHEDULE_SIZE];  //!< The event schedule buffer

EventContext ctx_led;              //!< Event handler context to turn LED on and off
EventContext ctx_print_once;       //!< Event handler context to print single timestamps
EventContext ctx_print_periodic;   //!< Event handler context to print periodic timestamps

int  led_state = 0;       //!< LED on/off state
bool add_error = false;   //!< Error flag for add() method in event handler


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * setup()
 **************************************/ 
void setup()
{
    // Hardware initialization.
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, led_state);
    Serial.begin(SERIAL_BAUD);
    delay(500);
    Serial.println("EventScheduler64 example using timer interrupts");
    Serial.print("Toggles LED at "); Serial.print(LED_PERIOD_US); Serial.println(" us intervals");
    Serial.print("Prints timestamps at "); Serial.print(PRINT_PERIOD_US); Serial.println(" us intervals");
    
    // Initialize the micros64 timer.
    if (!micros64_begin())
    {
        Serial.println("ERROR: micros64 initialization failed.");
        while (true) {}
    }
    
    // Delay 1 second to allow the micros64 timer to acquire a non-zero elapsed time.
    // This is done to we can schedule an out-of-order event below.
    delay(1000);
    
    // Scheduler initialization.
    if (!EventScheduler1.init(event_buf, SCHEDULE_SIZE))
    {
        Serial.println("ERROR: EventScheduler64 initialization failed.");
        while (true) {}
    }

    // Single print event context initialization.
    uint64_t now = micros64();
    ctx_print_once.handler = printOnceHandler;
    ctx_print_once.isr_run = false;
    
    // Schedule a bunch of single print events.
    for (uint32_t i = 1; i <= 10; i++)
    {
        ctx_print_once.start_time = now + (uint64_t)(i * 1000000);
        if (!EventScheduler1.add(&ctx_print_once))
        {
            Serial.print("ERROR scheduling single print event ");
            Serial.println(i);
        }
    }
    
    // Schedule an out-of-order event that occurred in the past.
    ctx_print_once.handler = pastHandler;
    ctx_print_once.start_time = now - 100;
    ctx_print_once.isr_run = false;
    if (!EventScheduler1.add(&ctx_print_once))
    {
        Serial.println("ERROR scheduling out-of-order print event");
    }

    // Periodic print event context initialization.
    ctx_print_periodic.handler    = printPeriodicHandler;
    ctx_print_periodic.start_time = micros64() + PRINT_PERIOD_US;
    ctx_print_periodic.isr_run    = false;
    if (!EventScheduler1.add(&ctx_print_periodic))
    {
        Serial.println("ERROR scheduling periodic print event");
    }
    
    // LED event context initialization.
    // The event occurs in an interrupt context.
    ctx_led.handler    = ledHandler;
    ctx_led.start_time = micros64() + LED_PERIOD_US;
    ctx_led.isr_run    = true;
    if (!EventScheduler1.add(&ctx_led))
    {
        Serial.println("ERROR scheduling LED event");
    }
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{
    // Run the event scheduler service routine at regular intervals.
    EventScheduler1.service();
    
    if (add_error)
    {
        add_error = false;
        Serial.println("ERROR scheduling LED event");
    }
}


/**************************************
 * ledHandler()
 **************************************/ 
void ledHandler()
{
    // Manage the LED.
    led_state = 1 - led_state;          // Toggle the LED state
    digitalWrite(LED_BUILTIN, led_state);   // Set the LED state
    
    // Schedule the next event.
    ctx_led.start_time = micros64() + LED_PERIOD_US;
    add_error = !EventScheduler1.add(&ctx_led);
}


/**************************************
 * printOnceHandler()
 **************************************/ 
void printOnceHandler()
{
    // Print the timestamp
    Serial.print("Single event time: ");
    Serial.print(millis());
    Serial.println(" ms");
}


/**************************************
 * printPeriodicHandler()
 **************************************/ 
void printPeriodicHandler()
{
    // Print the timestamp
    Serial.print("Periodic event time: ");
    Serial.print(millis());
    Serial.println(" ms");
    
    // Schedule the next event.
    ctx_print_periodic.start_time = micros64() + PRINT_PERIOD_US;
    if (!EventScheduler1.add(&ctx_print_periodic))
    {
        Serial.println("ERROR scheduling periodoc print event");
    }
}


/**************************************
 * pastHandler()
 **************************************/ 
void pastHandler()
{
    Serial.println("This event was scheduled to occur in the past");
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.