/******************************************************************************
 * event_scheduler_polled.ino
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Example of using the EventScheduler class in polled mode.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "EventScheduler.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
void ledHandler();    //!< Event handler to turn LED on and off
void printHandler();  //!< Event handler to print timestamps


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD   (115200)
#define SCHEDULE_SIZE      (4)
#define LED_BUILTIN       (13)
#define LED_PERIOD       (517)
#define PRINT_PERIOD    (2359)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
 
EventScheduler scheduler;  //!< The event scheduler

EventContext event_buf[SCHEDULE_SIZE];  //!< The event schedule buffer

EventContext ctx_led;      //!< Event handler context to turn LED on and off
EventContext ctx_print;    //!< Event handler context to print timestamps

int led_state = 0;         //!< LED on/off state for event handler


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * setup()
 **************************************/ 
void setup()
{
    // Hardware initialization.
    led_state = 0;
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, led_state);
    Serial.begin(SERIAL_BAUD);
    delay(1000);
    Serial.println("EventScheduler example in polled mode");
    Serial.print("Toggles LED at "); Serial.print(LED_PERIOD); Serial.println(" ms intervals");
    Serial.print("Prints timestamps at "); Serial.print(PRINT_PERIOD); Serial.println(" ms intervals");
    
    // Scheduler initialization.
    if (!scheduler.init(event_buf, SCHEDULE_SIZE, true))
    {
        Serial.println("ERROR: EventScheduler initialization failed.");
        while (true) {}
    }
    
    // LED event context initialization.
    ctx_led.handler    = ledHandler;
    ctx_led.start_time = millis() + LED_PERIOD;
    if (!scheduler.add(&ctx_led))
    {
        Serial.println("ERROR scheduling LED event");
    }
    
    // Print event context initialization.
    ctx_print.handler    = printHandler;
    ctx_print.start_time = millis() + PRINT_PERIOD;
    if (!scheduler.add(&ctx_print))
    {
        Serial.println("ERROR scheduling print event");
    }
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{
    // Run the event scheduler service routine at regular intervals.
    scheduler.service();
}


/**************************************
 * ledHandler()
 **************************************/ 
void ledHandler()
{
    // Manage the LED.
    led_state = 1 - led_state;              // Toggle the LED state
    digitalWrite(LED_BUILTIN, led_state);
    //Serial.print(millis()); Serial.print(" LED "); Serial.println(led_state);
    
    // Schedule the next event.
    ctx_led.start_time = millis() + LED_PERIOD;
    if (!scheduler.add(&ctx_led))
    {
        Serial.println("ERROR scheduling LED event");
    }
}


/**************************************
 * printHandler()
 **************************************/ 
void printHandler()
{
    // Print the timestamp
    Serial.print("Time: ");
    Serial.print(millis());
    Serial.println(" ms");
    
    // Schedule the next event.
    ctx_print.start_time = millis() + PRINT_PERIOD;
    if (!scheduler.add(&ctx_print))
    {
        Serial.println("ERROR scheduling print event");
    }
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.