/*****************************************************************************
 * EventScheduler.h
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * EventScheduler and EventContext class definition.  
 *
 * The EventScheduler class uses the Priority Queue library.  
 * It is part of the Queue repository and can be found here: 
 * https://gitlab.com/tkerr/queue
 */

#ifndef _EVENT_SCHEDULER_H_
#define _EVENT_SCHEDULER_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "PriorityQueue.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
/**
 * @brief Event handler callback function type definition.
 *
 * The event handler callback function takes no arguments and 
 * returns nothing. Example:
 *
 * @code
   void event_function(void);
   @endcode
 */
typedef void (*event_cb_fn)(void);


/****
 * Uncomment one of the following blocks to match the return type of the system 
 * time function used in your application.  For example, Arduino millis() and 
 * micros() return a uint32_t value.  micros64 returns a uint64_t value.
 ****/

// Uncomment these two lines if using a 32-bit time for system time (millis(), micros(), etc.).
// typedef uint32_t evt_time_t;
// #define EVENT_MAX_PERIOD = (0xFFFFFFFFUL)

// Uncomment these two lines if using a 64-bit time for system time (micros64(), etc.).
// Note: It should also be compatible with 32-bit system time functions.
typedef uint64_t evt_time_t; 
#define EVENT_MAX_PERIOD (0xFFFFFFFFFFFFFFFFULL)

 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/


/**
 * @brief
 * Event context class used by EventScheduler.
 *
 * Encapsulates an event handler, the event start time, and an interrupt 
 * service routine handler flag.
 *
 * The event handler function signature must correspond the @c event_cb_fn type.
 *
 * Event start time is the desired elapsed system time of the event.  The 
 * event handler function will be called when this time expires.
 *
 * Time units are application dependent but must be consistent (milliseconds, 
 * microseconds, system ticks, etc.)
 * 
 * In interrupt mode, the @c isr_run flag can be used to cause the event
 * handler to be run immediately in interrupt context instead of user
 * context during the next service loop.  Obviously this must be used
 * with extreme care.  This is meant for short, time-critial events
 * such as turning an output bit on or off at a very specific time.
 */
class EventContext
{
public:

    event_cb_fn handler;     //!< The event handler function
    evt_time_t  start_time;  //!< Desired start time of the event
    bool        isr_run;     //!< Run handler from timer ISR if true; use with extreme caution!
    
    EventContext();  //!< Default constructor
    
    /**
     * @brief The greater-than (>) operator.
     */
    bool operator>(const EventContext& other) const {return (start_time > other.start_time);}
    
    /**
     * @brief The less-than (<) operator.
     */
    bool operator<(const EventContext& other) const {return (start_time < other.start_time);}
    
    /**
     * @brief The equal-to (==) operator.
     * Not needed by the EventScheduler class, but provided for completeness.
     */
    bool operator==(const EventContext& other) const {return (start_time == other.start_time);}
    
    /**
     * @brief The not equal-to (!=) operator.
     * Not needed by the EventScheduler class, but provided for completeness.
     */
    bool operator!=(const EventContext& other) const {return (start_time != other.start_time);}
};


/**
 * @brief
 * Implements a simple timer-based event scheduler.
 *
 * Events are contained in an EventContext object and consist of an event handler
 * callback function and a start time.  Events are scheduled by adding them to 
 * the event handler.  When the event start time matches the elapsed system time 
 * (via the getTime() method), the event handler function is called.
 *
 * The scheduler can run in either polled or interrupt mode. In polled mode,
 * event time is checked in the service() method and all event handler functions 
 * are called from the this method. 
 *
 * In interrupt mode, a timer is set to expire at the event's start time and 
 * calls an interrupt handler. If the isr_run flag is set in the event context,
 * then the event handler function is called from the interrupt handler.
 * Otherwise, the event is run during the next call to service().  Running
 * an event handler from an interrupt can provide precise timing actions, 
 * but must be used with caution.  Event handlers should be short and perform
 * simple, specific actions such as setting or clearing a hardware pin.  The
 * isr_run flag is cleared by default and must be explicitly set by the user.
 *
 * The EventScheduler base class provides a polling mechanism using the
 * Arduino millis() function.  Interrupt modes must be implemented by
 * creating a subclass and overriding the virtual methods initTimer(),
 * setTimer() and clearTimer().  The getTime() method can also be
 * overriden in order to provide timing units consistent with the
 * interrupt timer (microseconds, system ticks, etc.).
 */
class EventScheduler
{
public:

    static evt_time_t MAX_PERIOD;
    
    /**
     * @brief Default constructor.
     */
    EventScheduler();
    
    /**
     * @brief Initialize the event scheduler.
     *
     * Call once during startup to initialize the object.
     *
     * @param buffer The memory buffer used to hold EventContext objects.  
     * This is provided by the caller so that they have control over how memory 
     * is allocated for it.
     *
     * @param max_events The maximum number of EventContext objects that can
     * be scheduled.  Note that the memory buffer provided must be large enough 
     * to hold these objects.
     *
     * @param polled True = event scheduler is polled, false = event scheduler 
     * is interrupt driven by timer interrupts.  If false, then a derived class
     * must provide initTimer(), setTimer() and clearTimer() methods. 
     * A compatible getTime() method may also be necessary.
     *
     * @return True if the event scheduler was initialized successfully, false otherwise.
     */
    bool init(EventContext* buffer, unsigned int max_events, bool polled);
    
    /**
     * @brief Periodic service routine. 
     *
     * Call at regular intervals within the main loop to run the event scheduler
     * and service event handlers.
     */
    void service();
    
    /**
     * @brief Add an event to the scheduler.
     *
     * @param context Pointer to an EventContext object containing the event
     * handler to schedule.
     *
     * @return True if the event was scheduled successfully, false otherwise.
     */
    bool add(const EventContext* context);
 
    /**
     * @brief Event scheduler interrupt service routine.
     *
     * For timer-driven interrupts, call this method from the derived class
     * timer ISR when the timer expires.
     *
     * This method is meant to be called only from derived class timer ISR.
     * It is not meant to be called directly by the user.
     */
    void isr();
    
    /**
     * @brief Return the system time used for event scheduling.
     *
     * The base class method returns the millis() count.  This method is intended 
     * to be overridden by a derived class for the specific application implementation.
     *
     * This method is used in both polled and interrupt modes.
     *
     * @return The current system time. Units are application dependent but must be
     * consistent with the setTimer() method (milliseconds, microseconds, system 
     * ticks, etc.)
     */
    virtual evt_time_t getTime();
    
protected:

    /**
     * @brief Initialize the application-specific timer used for event scheduling.
     *
     * This method should initialize the application-specific timer hardware, 
     * but the timer should not run until the setTimer() method is called.
     *
     * The base class method does nothing.  This method is intended to be 
     * overridden by a derived class for the specific application implementation.
     *
     * This method is not used in polled mode.  It is only used in interrupt mode.
     *
     * @return True if initialization was successful, false otherwise.
     */
    virtual bool initTimer();
    
    /**
     * @brief Set the application-specific timer period used for event scheduling.
     *
     * This method should start the timer and cause an interrupt to be generated
     * when the period expires.  Upon timer period expiration, the next event in 
     * the scheduler queue is ready to execute.
     *
     * The base class method does nothing.  This method is intended to be 
     * overridden by a derived class for the specific application implementation.
     *
     * This method is not used in polled mode.  It is only used in interrupt mode.
     *
     * @param start_time The desired event starting time. Units are application 
     * dependent but must be consistent with the getTime() method (milliseconds, 
     * microseconds, system ticks, etc.)
     *
     * @return True if timer set was successful, false otherwise.
     */
    virtual bool setTimer(evt_time_t start_time);
    
    /**
     * @brief Clear the application-specific timer used for event scheduling.
     *
     * This method should stop the timer so that it no longer generates interrupts.
     *
     * The base class method does nothing.  This method is intended to be 
     * overridden by a derived class for the specific application implementation.
     *
     * This method is not used in polled mode.  It is only used in interrupt mode.
     */
    virtual void clearTimer();

    bool       m_initialized;  //!< True if object is initialized, false otherwise
    bool       m_polled;       //!< True if using polled mode, false if using interrupt mode
    evt_time_t m_next_start;   //!< Start time of next event in the queue
    
    volatile unsigned int m_ready_count;  //!< The number of events ready for execution
    
    PriorityQueue<EventContext> m_event_queue;  //!< The scheduled event queue

private:
   
    void handleEvent();  //!< Check if an event is ready to run, and run it if so.
    void poll();         //!< Poll for next event in polling mode

};

#endif // _EVENT_SCHEDULER_H_
