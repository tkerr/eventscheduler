/******************************************************************************
 * EventScheduler64.cpp
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * EventScheduler64 class definition.  
 *
 * Defines an interrupt-driven event scheduler based on the EventScheduler 
 * base class, but uses IntervalTimerEx for timer interrupts and micros64
 * for system time.
 *
 * See EventScheduler64.h for details.
 */

// This class is only supported on Teensy platforms.
#include "arduino_arch.h"
#ifdef ARDUINO_ARCH_TEENSY


/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "EventScheduler64.h"
#include "IntervalTimerEx.h"
#include "micros64.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
static void timerIsr1(); //!< Static ISR function used by EventScheduler1
static void timerIsr2(); //!< Static ISR function used by EventScheduler2
static void timerIsr3(); //!< Static ISR function used by EventScheduler3


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define NUM_TIMERS (3)

class LocalTimer
{
public:
    IntervalTimerEx timer;
    bool            in_use;
    LocalTimer() : in_use(false) {}
};

typedef void (*timer_isr_t)();  //!< Interrupt service routine function signature


/******************************************************************************
 * Global objects and  data.
 ******************************************************************************/
EventScheduler64 EventScheduler1;  // Global event scheduler object 1
EventScheduler64 EventScheduler2;  // Global event scheduler object 2
EventScheduler64 EventScheduler3;  // Global event scheduler object 3


/******************************************************************************
 * Local data.
 ******************************************************************************/
 
/****
 * Arrays of IntervalTimerEx timers and their interrupt service routines.
 *
 * Up to 4 IntervalTimerEx objects may be active simultaneuously on Teensy 3.0 - 4.1. 
 * Teensy LC has only 2 timers for IntervalTimerEx. 
 * One timer is used for micros64(), leaving up to three for EventScheduler64 objects.
 * Timer hardware resources are not allocated until the EventScheduler64.init()
 * method is called.
 ****/
static LocalTimer  timer_array[NUM_TIMERS];
static timer_isr_t isr_array[NUM_TIMERS] = {timerIsr1, timerIsr2, timerIsr3};


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/

/**************************************
 * EventScheduler64::getTime
 **************************************/
evt_time_t EventScheduler64::getTime()
{
    return micros64();
}


/*****************************************************************************
 * Protected methods and functions.
 ******************************************************************************/
 
/**************************************
 * EventScheduler64::initTimer
 **************************************/
bool EventScheduler64::initTimer()
{
    // Make sure the system time return type is correct.
    if (sizeof(evt_time_t) < 8) return false;
    
    // Initialize the micros64() library.
    // Has no effect if already initialized.
    if (!micros64_begin()) return false;
    
    // Find an unused event timer and initialize it.
    // Initial timer period is arbitrary.
    m_timer_num = -1;
    for (int i = 0; i < NUM_TIMERS; i++)
    {
        if (!timer_array[i].in_use)
        {
            // Timer not in use for EventScheduler64.  
            // Try to initialize it.
            if (timer_array[i].timer.begin(isr_array[i], 10000000UL))
            {
                // Timer initialization successful.
                timer_array[i].timer.stop();
                timer_array[i].in_use = true;
                m_timer_num = i;
                break;
            }
        }
    }
    return (m_timer_num >= 0);
}


/**************************************
 * EventScheduler64::setTimer
 **************************************/ 
bool EventScheduler64::setTimer(evt_time_t start_time)
{
    if (m_timer_num < 0) return false;
    
    // Initialize to interrupt almost immediately
    // in case event occurs now or in the past.
    uint32_t period = 2;  

    evt_time_t now = micros64();
    if (start_time > now)
    {
        // Event occurs in the future.
        period = (uint32_t)(start_time - now);
    }
    bool ok = timer_array[m_timer_num].timer.setInterval(period);
    timer_array[m_timer_num].timer.start();
    
    return ok;
}


/**************************************
 * EventScheduler64::clearTimer
 **************************************/
void EventScheduler64::clearTimer()
{
    if (m_timer_num >= 0)
    {
        timer_array[m_timer_num].timer.stop();
    }
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/
 
/**************************************
 * timerIsr1
 **************************************/
static void timerIsr1()
{
    // Need to call the non-static ISR from a static ISR.
    EventScheduler1.isr();
}


/**************************************
 * timerIsr2
 **************************************/
static void timerIsr2()
{
    // Need to call the non-static ISR from a static ISR.
    EventScheduler2.isr();
}


/**************************************
 * timerIsr3
 **************************************/
static void timerIsr3()
{
    // Need to call the non-static ISR from a static ISR.
    EventScheduler3.isr();
}


#endif // ARDUINO_ARCH_TEENSY

// End of file.
