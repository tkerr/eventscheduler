/******************************************************************************
 * EventScheduler.cpp
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * EventScheduler and EventContext class implementation.
 *
 * See EventScheduler.h for details.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "EventScheduler.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and  data.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/

/**************************************
 * EventContext::EventContext
 **************************************/
EventContext::EventContext() :
    handler(0),
    start_time(EVENT_MAX_PERIOD),
    isr_run(false)
{
    // Nothing else to do.
}


/**************************************
 * EventScheduler::EventScheduler
 **************************************/
EventScheduler::EventScheduler() :
    m_initialized(false),
    m_polled(true),
    m_next_start(EVENT_MAX_PERIOD),
    m_ready_count(0)
{
    // Nothing else to do.
}


/**************************************
 * EventScheduler::init
 **************************************/
bool EventScheduler::init(EventContext* buffer, unsigned int max_events, bool polled)
{
    if (m_initialized) return true;
    if (buffer == 0) return false;
    m_polled = polled;
    m_next_start = EVENT_MAX_PERIOD;
    m_ready_count = 0;
    if (!m_event_queue.init(buffer, max_events, false)) return false;
    if (!polled)
    {
        if (!initTimer()) return false;
    }
    m_initialized = true;
    return true;
}


/**************************************
 * EventScheduler::service
 **************************************/
void EventScheduler::service()
{
    poll();
    handleEvent();
}


/**************************************
 * EventScheduler::add
 **************************************/
bool EventScheduler::add(const EventContext* context)
{
    bool ok = false;
    
    if (m_event_queue.available() > 0)
    {
        // Add the event to the queue.
        noInterrupts();
        ok = m_event_queue.push(context);
        interrupts();
        
        if (ok)
        {
            // Event successfully scheduled.
            // If interrupt mode, see if it has a sooner start time than current events.
            if (!m_polled && (context->start_time < m_next_start))
            {
                // Adjust timer for this event.
                setTimer(context->start_time);
                m_next_start = context->start_time;
            }
        }
    }
    
    return ok;
}


/**************************************
 * EventScheduler::isr
 **************************************/
void EventScheduler::isr()
{
    const EventContext* context;
    
    // Another event is ready for execution.
    m_ready_count++;
    
    // Check if there is another event that needs the timer set.
    context = m_event_queue.peek(m_ready_count);
    if (context != 0)
    {
        setTimer(context->start_time);
        m_next_start = context->start_time;
    }
    else
    {
        // No scheduled events in the queue.
        clearTimer();
        m_next_start = EVENT_MAX_PERIOD;
    }
    
    // Check if we need to run the event handler within the ISR.
    context = m_event_queue.peek(0);
    if (context != 0)
    {
        if (context->isr_run)
        {
            handleEvent();
        }
    }
}


/**************************************
 * EventScheduler::getTime
 **************************************/
evt_time_t EventScheduler::getTime()
{
    // Base class returns elapsed millisecond count.
    // Derived classes can override this method.
    return (evt_time_t)millis();
}


/*****************************************************************************
 * Protected methods and functions.
 ******************************************************************************/
 
/**************************************
 * EventScheduler::initTimer
 **************************************/
bool EventScheduler::initTimer()
{
    // Base class does nothing.
    // Override this method in a derived class.
    return true;
}


/**************************************
 * EventScheduler::setTimer
 **************************************/ 
bool EventScheduler::setTimer(evt_time_t start_time)
{
    // Base class does nothing.
    // Override this method in a derived class.
    return true;
}


/**************************************
 * EventScheduler::clearTimer
 **************************************/
void EventScheduler::clearTimer()
{
    // Base class does nothing.
    // Override this method in a derived class.
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/

/**************************************
 * EventScheduler::handleEvent
 **************************************/
void EventScheduler::handleEvent()
{
    EventContext context;
    bool ok;
    
    // See if an event is ready for execution.
    if (m_ready_count > 0)
    {
        // Remove the event from the queue.
        noInterrupts();
        m_ready_count--;
        ok = m_event_queue.pop(&context);
        interrupts();
        
        // Execute the event handler.
        if (ok && (context.handler != 0))
        {
            context.handler();
        }
    }
}


/**************************************
 * EventScheduler::poll
 **************************************/
void EventScheduler::poll()
{
    const EventContext* context;
    
    // If polling mode, then poll for the next event.
    // Note: No interrupts are expected in polled mode.
    if (m_polled)
    {
        context = m_event_queue.peek(0);
        
        // See if there is another scheduled event.
        if (context != 0)
        {
            // Found another scheduled event.
            // See if it is ready for execution.
            if (context->start_time <= getTime())
            {
                // Another sheduled event is ready.
                // Execute it during the next call to handleEvent().
                m_ready_count++;
            }
        }
        else
        {
            // No scheduled events.
            m_next_start = EVENT_MAX_PERIOD;
        }
    }
}


// End of file.
