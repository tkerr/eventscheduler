/*****************************************************************************
 * EventScheduler64.h
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * EventScheduler64 class definition.  
 *
 * Defines an interrupt-driven event scheduler based on the EventScheduler 
 * base class, but uses IntervalTimerEx for timer interrupts and micros64
 * for system time.
 *
 * Events can be scheduled with microsecond precision without fear of system 
 * time rollover.
 *
 * The IntervalTimerEx library is currently only supported on
 * Teensy platforms from PJRC.  See https://www.pjrc.com/teensy/.
 *
 * The IntervalTimerEx library can be found here:
 * https://gitlab.com/tkerr/IntervalTimerEx
 *
 * The micros64 library can be found here:
 * https://gitlab.com/tkerr/micros64
 */

#ifndef _EVENT_SCHEDULER64_H_
#define _EVENT_SCHEDULER64_H_

// This class is only supported on Teensy platforms.
#include "arduino_arch.h"
#ifdef ARDUINO_ARCH_TEENSY


/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "EventScheduler.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @brief
 * Event scheduler using the IntervalTimerEx class for interrupt-based 
 * scheduling and the micros64 library for system time.  Events can be
 * scheduled with microsecond precision without fear of system time rollover.
 *
 * Up to 4 IntervalTimerEx objects may be active simultaneuously on Teensy 3.0 - 4.1. 
 * Teensy LC has only 2 timers for IntervalTimerEx. 
 * One timer is used for micros64(), leaving up to three for EventScheduler64 objects.
 * Timer hardware resources are not allocated until the EventScheduler64.init()
 * method is called.
 */
class EventScheduler64 : public EventScheduler
{
public:

    /**
     * @brief Default constructor.
     */
    EventScheduler64() : EventScheduler(), m_timer_num(-1) {}
    
    /**
     * @brief Initialize the event scheduler.
     *
     * Call once during startup to initialize the object.
     *
     * @param buffer The memory buffer used to hold EventContext objects.  
     * This is provided by the caller so that they have control over how memory 
     * is allocated for it.
     *
     * @param max_events The maximum number of EventContext objects that can
     * be scheduled.  Note that the memory buffer provided must be large enough 
     * to hold these objects.
     *
     * @return True if the event scheduler was initialized successfully, false otherwise.
     */
    bool init(EventContext* buffer, unsigned int max_events) {return EventScheduler::init(buffer, max_events, false);}
    
    /**
     * @brief Return the system time used for event scheduling.
     *
     * Overrides the base class virtual method to use the micros64 library.
     *
     * @return The current system time as a 64-bit elapsed microsecond count.
     */
    evt_time_t getTime();
    
protected:

    /**
     * @brief Initialize the application-specific timer used for event scheduling.
     *
     * Overrides the base class virtual method to use the IntervalTimerEx library.
     *
     * @return True if initialization was successful, false otherwise.
     */
    bool initTimer();
    
    /**
     * @brief Set the application-specific timer period used for event scheduling.
     *
     * Overrides the base class virtual method to use the IntervalTimerEx library.
     *
     * @param start_time The desired event starting time. Units are 64-bit elapsed
     * microseconds, which is compatible with the getTime() method.  Note that due
     * to numeric limits, the maximum start time is approximately 71 seconds into
     * the future.
     *
     * @return True if timer set was successful, false otherwise.
     */
    bool setTimer(evt_time_t start_time);
    
    /**
     * @brief Clear the application-specific timer used for event scheduling.
     *
     * Overrides the base class virtual method to use the IntervalTimerEx library.
     */
    void clearTimer();
    
private:

    int m_timer_num;  //!< Timer number allocated for use by this object
};

/****
 * Up to 4 IntervalTimerEx objects may be active simultaneuously on Teensy 3.0 - 4.1. 
 * Teensy LC has only 2 timers for IntervalTimerEx. 
 * One timer is used for micros64(), leaving up to three for EventScheduler64 objects.
 * Timer hardware resources are not allocated until the EventScheduler64.init()
 * method is called.
 ****/
extern EventScheduler64 EventScheduler1;  //!< Global event scheduler object 1
extern EventScheduler64 EventScheduler2;  //!< Global event scheduler object 2
extern EventScheduler64 EventScheduler3;  //!< Global event scheduler object 3

#endif // ARDUINO_ARCH_TEENSY

#endif // _EVENT_SCHEDULER64_H_
